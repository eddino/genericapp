import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {from, Observable,} from "rxjs";
import { environment } from "../../environments/environment"
import { ProvaindirizzoComponent } from './provaindirizzo.component';
import { map } from 'rxjs/operators';

export interface MapBoxOutPut{
  attribution: string;
  features: Feature[];
  query: [];
}

export interface Feature{
  place_name: string;
}

@Injectable({
  providedIn: 'root'
})
export class MapboxServiceService {

  constructor(private http : HttpClient) { }


  search_word(query:string){
  const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
  return this.http.get(url+query+'.json?types=address&access_token='
  + environment.mapbox.accessToken
  )
  .pipe(map((res: MapBoxOutPut)=>{
    return res.features;
  }))
}

}
