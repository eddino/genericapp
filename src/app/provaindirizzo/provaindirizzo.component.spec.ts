import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvaindirizzoComponent } from './provaindirizzo.component';

describe('ProvaindirizzoComponent', () => {
  let component: ProvaindirizzoComponent;
  let fixture: ComponentFixture<ProvaindirizzoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProvaindirizzoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvaindirizzoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
