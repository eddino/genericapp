import { Component, OnInit } from '@angular/core';
import {from, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import { MapboxServiceService,Feature} from "./mapbox-service.service"
import { features } from 'process';
@Component({
  selector: 'app-provaindirizzo',
  templateUrl: './provaindirizzo.component.html',
  styleUrls: ['./provaindirizzo.component.css']
})
export class ProvaindirizzoComponent implements OnInit {

  constructor(private mapbox: MapboxServiceService) { }

  addresses : string[] = [];
  selectedAddress = null;

  search(event : any){
    const searchTerm = event.target.value.toLowerCase();
    if(searchTerm && searchTerm.length>0){
      this.mapbox
      .search_word(searchTerm)
      .subscribe((features : Feature[])=>{
        this.addresses = features.map(feat => feat.place_name)
      })
    }else{
      this.addresses = [];
    }
  }

  onSelect(address:string){
    this.selectedAddress = address;
    this.addresses = [];
  }

  ngOnInit(): void {
  }

}
