import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AmministratoreComponent } from './amministratore/amministratore.component';
import { MagazzinoComponent } from './amministratore/pagina-interna-amm/magazzino/magazzino.component';
import { OrdiniComponent } from "./amministratore/pagina-interna-amm/ordini/ordini.component";
import { PaginaInternaAmmComponent } from './amministratore/pagina-interna-amm/pagina-interna-amm.component';
import { RegistrazioneComponent } from './amministratore/registrazione/registrazione.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProvaindirizzoComponent } from './provaindirizzo/provaindirizzo.component';

const routes: Routes = [
  { path: 'homepage', component: HomepageComponent },

  // Inizio Routing Amministratore
  { path: 'amministratore', component: AmministratoreComponent },
  { path: '', component: HomepageComponent },
  {path:'indirizzo',component:ProvaindirizzoComponent},
  { path: '', redirectTo: 'homepage', pathMatch: 'full' },
  {path:'partners',component:RegistrazioneComponent},
  {
    path: 'gestione-attivita', component: PaginaInternaAmmComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
