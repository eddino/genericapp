import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleDettaglioOrdineComponent } from './modale-dettaglio-ordine.component';

describe('ModaleDettaglioOrdineComponent', () => {
  let component: ModaleDettaglioOrdineComponent;
  let fixture: ComponentFixture<ModaleDettaglioOrdineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleDettaglioOrdineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleDettaglioOrdineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
