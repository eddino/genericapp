import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginatorIntl } from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import { ModaleDettaglioOrdineComponent } from './modale-dettaglio-ordine/modale-dettaglio-ordine.component';
@Component({
  selector: 'app-ordini',
  templateUrl: './ordini.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['./ordini.component.css']
})
export class OrdiniComponent implements OnInit {
  displayedColumns: string[] = ['codiceOrdine', 'utente', 'ritiroProdotto', 'dataOrdine','dataConsegna','actions'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  constructor(public dialog: MatDialog) { }
  @ViewChild(MatPaginator, {static:true} )paginator: MatPaginator;
  ngOnInit(): void {
    this.paginator._intl.itemsPerPageLabel="Elementi per pagina";
  }

  openModalDetail(){
    const dialogRef = this.dialog.open(ModaleDettaglioOrdineComponent);
  }

  

}

export interface PeriodicElement {
  utente: string;
  codiceOrdine: number;
  ritiroProdotto: number;
  dataOrdine: string;
  dataConsegna: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {codiceOrdine: 1, utente: 'Hydrogen', ritiroProdotto: 1.0079, dataOrdine: 'H',dataConsegna: 'H'},
  {codiceOrdine: 2, utente: 'Helium', ritiroProdotto: 4.0026, dataOrdine: 'He',dataConsegna: 'H'},
  {codiceOrdine: 3, utente: 'Lithium', ritiroProdotto: 6.941, dataOrdine: 'Li',dataConsegna: 'H'},
  {codiceOrdine: 4, utente: 'Beryllium', ritiroProdotto: 9.0122, dataOrdine: 'Be',dataConsegna: 'H'},
  {codiceOrdine: 5, utente: 'Boron', ritiroProdotto: 10.811, dataOrdine: 'B',dataConsegna: 'H'},
  {codiceOrdine: 6, utente: 'Carbon', ritiroProdotto: 12.0107, dataOrdine: 'C',dataConsegna: 'H'},
  {codiceOrdine: 7, utente: 'Nitrogen', ritiroProdotto: 14.0067, dataOrdine: 'N',dataConsegna: 'H'},
  {codiceOrdine: 8, utente: 'Oxygen', ritiroProdotto: 15.9994, dataOrdine: 'O',dataConsegna: 'H'},
  {codiceOrdine: 9, utente: 'Hydrogen', ritiroProdotto: 1.0079, dataOrdine: 'H',dataConsegna: 'H'},
  {codiceOrdine: 10, utente: 'Helium', ritiroProdotto: 4.0026, dataOrdine: 'He',dataConsegna: 'H'},


  
];