import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleMagazzinoComponent } from './modale-magazzino.component';

describe('ModaleMagazzinoComponent', () => {
  let component: ModaleMagazzinoComponent;
  let fixture: ComponentFixture<ModaleMagazzinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleMagazzinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleMagazzinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
