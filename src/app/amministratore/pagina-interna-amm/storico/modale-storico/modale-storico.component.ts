import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginatorIntl } from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
@Component({
  selector: 'app-modale-storico',
  templateUrl: './modale-storico.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['./modale-storico.component.css']
})
export class ModaleStoricoComponent implements OnInit {

  displayedColumns: string[] = ['nomeProdotto', 'quantita', 'prezzo'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);


  constructor() { }

  ngOnInit(): void {
  }

}



export interface PeriodicElement{
  nomeProdotto: number;
  quantita:number;
  prezzo:number;

}
const ELEMENT_DATA: PeriodicElement[] = [
  {nomeProdotto: 1.0079,quantita:2,prezzo:29,},
  {nomeProdotto: 4.0026,quantita:2,prezzo:29,  },
  { nomeProdotto: 6.941,quantita:2,prezzo:29,  },
  {nomeProdotto: 9.0122,quantita:2,prezzo:29,  },
  {nomeProdotto: 10.811,quantita:2,prezzo:29, },

  
];