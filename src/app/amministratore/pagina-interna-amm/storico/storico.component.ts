import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginatorIntl } from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import { ModaleStoricoComponent } from './modale-storico/modale-storico.component';

@Component({
  selector: 'app-storico',
  templateUrl: './storico.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['./storico.component.css']
})
export class StoricoComponent implements OnInit {

  displayedColumns: string[] = ['codiceOrdine', 'utente', 'subTotale','actions'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  constructor(public dialog: MatDialog) { }
  @ViewChild(MatPaginator, {static:true} )paginator: MatPaginator;

  ngOnInit(): void {
    this.paginator._intl.itemsPerPageLabel="Elementi per pagina";
  }

  openModalDetail(){
    const dialogRef = this.dialog.open(ModaleStoricoComponent);
  }

  

}
export interface PeriodicElement {
  utente: string;
  codiceOrdine: number;
  subTotale: number;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {codiceOrdine: 1, utente: 'Hydrogen', subTotale: 1.0079},
  {codiceOrdine: 2, utente: 'Helium', subTotale: 4.0026},
  {codiceOrdine: 3, utente: 'Lithium', subTotale: 6.941},
  {codiceOrdine: 4, utente: 'Beryllium', subTotale: 9.0122},
  {codiceOrdine: 5, utente: 'Boron', subTotale: 10.811},
  {codiceOrdine: 6, utente: 'Carbon', subTotale: 12.0107},
  {codiceOrdine: 7, utente: 'Nitrogen', subTotale: 14.0067},
  {codiceOrdine: 8, utente: 'Oxygen', subTotale: 15.9994},

  
];